
<div class="col-md-12">
    <div class="card">
        <!-- <div class="header"> -->
            <!-- <h4 class="title">Daftar Kelas</h4> -->
        <!-- </div> -->

        <div class="content table-full-width">
            <!-- content  -->
            <table id="table">
              <thead>
                <tr>
                  <th>Hari</th>
                  <th>Makul</th>
                  <th>Dosen</th>
                  <th width="50" align="center">#</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($kelas as $key) :
                  $jadwal = $this->akademik->get_jadwal($key->kelas, $key->kode_makul, $this->session->userdata('tahun_akademik'));
                  ?>  
                <tr>
                  <td><?php echo ucwords($jadwal->hari);?></td>
                  <td><?php echo ucwords($key->nama_makul);?></td>
                  <td><?php echo ucwords($jadwal->nama_dosen);?></td>
                  <td align="center"><a href="<?php echo base_url()?>kelas/scanner/<?php echo $jadwal->id_jadwal?>"><i class="pe-7s-look"></i></a></td>
                </tr>
                <?php endforeach?>
              </tbody>
            </table>    
        </div>
    </div>
</div>

<script>
  $(document).ready(function(){
    $.ajax({
      url:"<?php echo base_url()?>kelas/ajax_load_jadwal",
      dataType: "html",
      success: function(html) {
        console.log(html);
        $('#show-data').html(html);
      },
      error: function (err) {
        console.error(err);
      }

    });
  });
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-container-breakpoint').basictable({
        containerBreakpoint: 485
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
  </script>