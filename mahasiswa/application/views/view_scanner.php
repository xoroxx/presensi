
<div class="col-md-6 col-md-offset-3">
    <div class="card">
        <!-- <div class="header"> -->
            <!-- <h4 class="title">Daftar Kelas</h4> -->
        <!-- </div> -->

        <div class="content">
            <!-- content  -->
            <canvas style="width: 100%; height: 100%;" id="webcodecam-canvas"></canvas>
            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    var arg = {
      // grayScale: 0,
      frameRate: 25,
      // contrast: 50,
      zoom: -1,
      beep: '<?php echo base_url()?>assets/audio/beep.mp3',
      decoderWorker: '<?php echo base_url()?>assets/js/DecoderWorker.js',
      // flipHorizontal: true,
      resultFunction: function(res) {
         doPresensi(res.code);
      }
    };

    //Initialize new Class
    var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
    decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg);
    //if Button click
  
      decoder.play();
   
    //do save presensi data
    function doPresensi(data) {
      $.ajax({
        url: "<?php echo base_url()?>kelas/do_presensi",
        data: {'data':data},
        type: "post",
        dataType:"json",
        success: function (response) {
          if(response.result == true){
            alert('SUCCESS');
          }else if(response.result == false){
            alert('Presensi Sudah Tersimpan');
          }
        },
        error: function (error) {
          console.log(error)
        }
      });
    }
  });
</script>