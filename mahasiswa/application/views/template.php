<!DOCTYPE HTML>
<html>
    <head>
        <!-- meta keywords specific to the page -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        {_meta}
        <!-- page title specific to the page -->
        <title>Aplikasi Presensi Ujian</title>
        <!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
        <!-- favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/logo.png'); ?>"/>
        <!-- template style -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css'); ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/basictable.css'); ?>">
        <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet" />
        <!-- style sheets specific to the page -->
        {_styles}
        <!-- js files specific to the page -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script> -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.basictable.min.js'); ?>"></script>
        {_scripts}
        <script type="text/javascript" src="<?php echo base_url('assets/js/qrcodelib.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/webcodecamjquery.js'); ?>"></script> 
    </head>
    <body>
        <div class="wrapper">
          <!-- sidebar -->
          <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-5.jpg'); ?>">
            <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->
        	  <div class="sidebar-wrapper">
              <div class="logo">
                <a href="#" class="simple-text"><?php echo strtoupper($this->session->userdata('nama'));?></a>
              </div>
              <ul class="nav">

                <li>
                  <a href="<?php echo base_url('kelas'); ?>">
                    <i class="pe-7s-note2"></i>
                    <p>Jadwal</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('kartu/view'); ?>">
                    <i class="pe-7s-id"></i>
                    <p>Kartu Ujian</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('kartu/print'); ?>" >
                    <i class="pe-7s-print"></i>
                    <p>Cetak Kartu</p>
                  </a>
                </li>
                <!-- <li>
                  <a href="<?php echo base_url('profile'); ?>" >
                    <i class="pe-7s-users"></i>
                    <p>Profile</p>
                  </a>
                </li> -->
                <li>
                  <a href="<?php echo base_url('logout'); ?>">
                    <i class="pe-7s-power"></i>
                    <p>Logout</p>
                  </a>
                </li>
              </ul>
        	   </div>
           </div>
          <!-- main panel -->
          <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><?php echo strtoupper($header_content);?></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <!-- <ul class="nav navbar-nav navbar-left">


                            <li>

                                    <a href="#"><i class="fa fa-search"></i></a>

                            </li>
                        </ul> -->

                        <!-- <ul class="nav navbar-nav navbar-right"> -->
                            <!-- <li>
                               <a href="">
                                   Account
                                </a>
                            </li> -->
                            <!-- <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                      <i class="fa fa-users"></i>
                                      Petugas Akademik
                                      <b class="caret"></b>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo base_url('akademik/logout'); ?>">Logout</a></li>
                                  </ul>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </nav>
            <!-- content specific to the page -->
            <div class="content">
                <!-- <div class="container-fluid"> -->
                    <div class="row">
                      {content}
                    </div>
                <!-- </div> -->
            </div>

            <!-- footer -->
            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-left">
                        Presensi Ujian
                    </p>
                    <p class="copyright pull-right">
                        &copy; 2018
                    </p>
                </div>
            </footer>
          </div>
        </div>
        <!-- js file -->

        <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/filereader.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/qrcodelib.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/webcodecamjs.js'); ?>"></script> -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js'); ?>"></script>
        <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/main.js'); ?>"></script> -->

    </body>
</html>
