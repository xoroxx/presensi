<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/**
* Usage example:
*/
 $this->load->library('rsa');
 $keys = $this->rsa->generate_keys();
 $message="hairoes";
 for ($i=32;$i<127;$i++) $message.=chr($i);
 $encoded = $this->rsa->rsa_encrypt ($message,  $keys[1],  $keys[0]);
 $decoded = $this->rsa->rsa_decrypt($encoded,  $keys[2],  $keys[0]);
 echo "<pre><br><i>Test ASCII(32) - ASCII(126):</i>\n";
 echo "Message: <b>$message</b>\n";
 echo "Encoded: <b>$encoded</b>\n";
 echo "Decoded: <b>".$decoded."</b>\n";
 echo "Decoded: <b>".sha1($decoded)."</b>\n";
		// $this->load->view('welcome_message');
	}
}
