<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keys extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_mahasiswa', 'mhs');
    if ($this->session->userdata('isLogin') == FALSE) {
      redirect('login');
    }
  }

  /**
   * [view_list_kelas controller]
   * @return [type] [description]
   */
  public function check_keys()
  {
    $nim = $this->session->userdata('nim');
    $this->get_smt_aktif($nim);
    $data['kelas'] = $this->mhs->get_kelas($nim, $this->session->userdata('smt_aktif'));
    $data['mahasiswa'] = $this->session->userdata('nama');
    $this->template->write_view('content', 'view_kartu_ujian', $data, TRUE);
    $this->template->render();
  }

  /**
   * [get_smt_aktif description]
   * @param  string $nim  [description]
   * @return session      [description]
   */
  public function get_smt_aktif($nim = '')
  {
    $result = $this->mhs->get_mahasiswa($nim);
    $angkatan = $result->angkatan;
    $thn_akademik  = $this->mhs->get_tahun_akademik(TRUE);
    if (isset($thn_akademik)) {
      $temp   = (substr($thn_akademik->tahun, 5) - $angkatan) * 2;
      $smt_aktif = $thn_akademik->keterangan == 1 ? $temp-1 : $temp;
      $this->session->set_userdata('smt_aktif', $smt_aktif);
    }
  }
}
