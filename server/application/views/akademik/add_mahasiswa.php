<!-- <div class="clearfix"></div> -->
<a href="<?php echo base_url() ?>mahasiswa" class="btn btn-sm btn-warning" ><i class='fa fa-mail-reply-all'></i>Kembali</a>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Mahasiswa</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url('mahasiswa/add') ?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nim">Nim<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="nim">
            </div>
          </div>
          
          <div class="form-group">
            <label for="nama" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input class="form-control col-md-7 col-xs-12" type="text" name="nama">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Prodi<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="prodi" class="form-control col-md-7 col-xs-12" required="required">
                <option value="informatika">Informatika</option>
                <option value="teknik informasi">Teknik Informasi</option>
                <option value="rpl">RPL</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Angkatan<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="angkatan" id="" class="form-control col-md-7 col-xs-12" required="required">
              <?php 
                $now = date('Y');
                for ($i= $now; $i > 2010; $i--) { 
                  echo "<option value=".$i."?>".$i."</option>";
                }
               ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelas<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="kelas" class="form-control col-md-7 col-xs-12" required="required">
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary" name="submit">Tambah</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>




            