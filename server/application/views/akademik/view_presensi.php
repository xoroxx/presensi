
<div class="">
  
  <div class="clearfix"></div>
  
 
  <div class="x_panel">
    <div class="row x_title">
      <div class="col-md-12">
        <h3>Data Presensi</h3>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label class="control-label col-md-12">Tahun Akademik</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="tahun-akademik" class="form-control" required="required" >
                <?php foreach ($tahun_akademik as $key ): ?>
                  <option value="<?php echo $key->id_tahun_akademik ?>"><?php echo $key->tahun ?> - <?php echo $key->periode==1?'Ganjil':'Genap' ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-12">Mata Kuliah</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="makul" class="form-control" required="required" onchange="clear_kelas()">
                <?php foreach ($makul as $key ): ?>
                  <option value="<?php echo $key->kode_makul?>"><?php echo $key->nama_makul?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <button class="btn btn-sm btn-primary" style="width: 100%; margin-top: 20px" onclick="load_kelas()">TAMPIL KELAS</button>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-12">Kelas</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="kelas" class="form-control" required="required" multiple="" >
                
              </select>
            </div>
          </div>

      </div>
       <div class="col-md-9 col-sm-12 col-xs-12" id="data-presensi">
            
              
                
                
        </div>
    </div>
    </div>

    
  </div>
</div>

<script type="text/javascript">
  function clear_kelas() {
    $("[name=kelas]").html('');
  }
function load_kelas()
{
    var tahun=$("[name=tahun-akademik]").val();
    var makul=$("[name=makul]").val();
    $.ajax({
      url:"<?php echo base_url()?>ajax/ajax_akademik/load_data_kelas",
      type: "post",
      data:{"tahun":tahun, "makul":makul},
      success: function(html)
         {
            console.log(html);
            $("[name=kelas]").html(html);
         },
      error: function (html) {
        console.error(html);
        // body...
      }
    });
}

function load_data_presensi(id,tahun)
{
  $.ajax({
    url:"<?php echo base_url()?>ajax/ajax_akademik/load_data_presensi",
    type: "post",
    data:{"id":id, 'tahun':tahun},
    success: function(html)
    {
      console.log(html);
      $("#data-presensi").html(html);
    }
  });
}

// function print_presensi(id) {
//   $.ajax({
//     url:"<?php echo base_url()?>ajax/ajax_presensi/print_presensi",
//     type: "post",
//     data:{"id":id},
//     success: function(data)
//     {
//       console.log(data);
//     }
//   });
// }
</script>
