<!-- <div class="clearfix"></div>  -->
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Jadwal</small></h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br /> 
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url('jadwal/add') ?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kelas
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <select name="nama" id="" class="form-control">
                 <?php 
                $no = A;
                for ($i=1; $i < chr(ord($no)); $i++) { 
                  echo "<option value=".$i.">".$i."</option>";
                }
                 ?> 
              </select>
            </div>
          </div>
          
          <div class="form-group">
            <label for="makul" class="control-label col-md-3 col-sm-3 col-xs-12">Mata Kuliah</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <!-- <select name="makul" id="" class="form-control">
                
                <?php foreach ($makul as $key): ?>
                  <option value="<?php echo $key->kode_makul ?>"><?php echo $key->nama_makul ?></option>
                <?php endforeach ?>
              </select> -->
            </div>
          </div>
          <div class="form-group">
            <label for="dosen" class="control-label col-md-3 col-sm-3 col-xs-12">Dosen</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
             <!--  <select name="dosen" id="" class="form-control">
                <?php foreach ($dosen as $key): ?>
                  <option value="<?php echo $key->kode_dosen ?>"><?php echo $key->nama_dosen ?></option>
                <?php endforeach ?>
              </select> -->
            </div>
          </div>
<div class="form-group">
            <label for="prodi" class="control-label col-md-3 col-sm-3 col-xs-12">Prodi</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <!-- <select name="prodi" id="" class="form-control">
                <?php foreach ($prodi as $key): ?>
                  <option value="<?php echo $key->prodi ?>"><?php echo $key->prodi ?></option>
                <?php endforeach ?>
              </select> -->
            </div>
          </div>    
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary" name="submit">Tambah</button>
            </div>
          </div>

        </form> 
     </div>
    </div>
  </div>
</div>




   