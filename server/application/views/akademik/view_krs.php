
<div class="">
  
  <div class="clearfix"></div>
  
 
  <div class="x_panel">
    <div class="row x_title">
      <div class="col-md-12">
        <h3>Kartu Rencana Studi</h3>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label class="control-label col-md-12">Prodi</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="prodi" class="form-control" required="required" onchange="loadmahasiswa()">
                <?php foreach ($prodi as $key ): ?>
                  <option value="<?php echo $key->prodi ?>"><?php echo $key->prodi ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-12">Angkatan</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="angkatan" class="form-control" required="required" onchange="loadmahasiswa()">
                <?php foreach ($angkatan as $key ): ?>
                  <option value="<?php echo $key->angkatan ?>"><?php echo $key->angkatan ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>

         <div class="form-group">
            <label class="control-label col-md-12">Nim / Nama</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="mahasiswa" class="form-control" required="required" multiple="">
                
              </select>
            </div>
          </div>
      </div>
       <div class="col-md-9 col-sm-12 col-xs-12" id="data-krs">
            
              
                
                
        </div>
    </div>
    </div>

    
  </div>
</div>

<script type="text/javascript">
function loadmahasiswa()
{
    var prodi=$("[name=prodi]").val();
    var angkatan=$("[name=angkatan]").val();
    $.ajax({
      url:"<?php echo base_url()?>ajax/ajax_mahasiswa/load_mahasiswa",
      type: "post",
      data:{"prodi":prodi, "angkatan":angkatan },
      success: function(html)
         {
            console.log(html);
            $("[name=mahasiswa]").html(html);
         },
      error: function (html) {
        console.error(html);
        // body...
      }
    });
}

function load_data_krs(nim)
{
  $.ajax({
    url:"<?php echo base_url()?>ajax/ajax_krs/load_data_krs",
    type: "post",
    data:{"nim":nim},
    success: function(html)
    {
      console.log(html);
      $("#data-krs").html(html);
    }
  });
}

function loadtablemapel(nim)
{
    $.ajax({
    url:"<?php echo base_url();?>ajax/ajax_krs/load_makul",
    data:{'nim':nim},
    type:"post",
    success: function(html)
    {
      $("#data-krs").html(html); 
    }
    });
}

function ambil(nim, tahun, kode)
{
  $.ajax({
  url:"<?php echo base_url();?>ajax/ajax_krs/ajax_save_krs",
  data:{'nim':nim,'tahun':tahun, 'makul':kode},
  type:"post",
  success: function(html)
  {
    $("#hide"+kode).hide(300);   
  }
  });
   
}
$(document).ready(function() {
  loadmahasiswa();
});
</script>
