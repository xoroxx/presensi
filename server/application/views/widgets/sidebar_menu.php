<?php if ($this->session->userdata('admin_loggin')): ?>
  
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>Master</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-university"></i> Akademik <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">

          <li><a href="<?php echo base_url()?>jadwal">Jadwal</a></li>
          <li><a href="<?php echo base_url()?>krs">KRS</a></li>
          <li><a href="<?php echo base_url()?>makul">Mata Kuliah</a></li>
          <li><a href="<?php echo base_url()?>keuangan">Keuangan</a></li>
          <!-- <li><a href="<?php echo base_url()?>prodi">Prodi</a></li> -->
          <li><a href="<?php echo base_url()?>akademik/view_tahun_akademik">Tahun Akademik</a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url()?>mahasiswa" ><i class="fa fa-graduation-cap"></i> Mahasiswa</a>
       <!--  <ul class="nav child_menu">
          <?php foreach ($prodi as $key): ?>
            <li><a href="<?php echo base_url()."mahasiswa/view/".$key->id_prodi."" ?>"><?php echo ucwords($key->nama_prodi) ?></a></li>
          <?php endforeach ?>
        </ul> -->
      </li>
      <li><a href="<?php echo base_url()?>dosen" ><i class="fa fa-briefcase"></i> Dosen </a>
        <!-- <ul class="nav child_menu">
          <li><a href="<?php echo base_url()?>dosen/add">Tambah Data</a></li>
          <li><a href="<?php echo base_url()?>dosen">List Data</a></li>
        </ul> -->
      </li>
      
      <li><a href="<?php echo base_url()?>user"><i class="fa fa-users"></i> Users </a>
      </li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Report</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url() ?>presensi"><i class="fa fa-list-alt"></i> Presensi </a>
      </li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Tools</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url() ?>akademik/setting"><i class="fa fa-cog"></i> Setting App <span class="label label-warning pull-right">Urgent</a>
      </li>
    </ul>
  </div>
  
<div class="menu_section">
    <h3>Setting On</h3>
    <ul class="nav side-menu">
      <li><a><?php echo $this->session->userdata('tahun_akademik') ?></a>
      </li>
    </ul>
  </div>
  



</div>

<?php else :?>

   
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>Menu</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url()?>dosen/presensi"><i class="fa fa-database"></i> Data Presensi</a>
        <li><a href="<?php echo base_url()?>dosen/scanning"><i class="fa fa-qrcode"></i> QR Scanner </a>
    </ul>
  </div>
 



</div>

<?php endif ?>