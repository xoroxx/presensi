<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_prodi extends CI_Model{

  protected $table = 'prodi';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete($id)
  {
    $this->db->where('id_prodi', $id);
    $this->db->delete($this->table);
  }

  public function add_prodi()
  {
    $nama      = $this->input->post('nama');
    $data = array('nama_prodi' => ucwords($nama));
    return $this->db->insert($this->table, $data);
  }

  public function get_prodi()
  {
    $sql = "SELECT prodi FROM mahasiswa GROUP BY prodi";
    return $this->db->query($sql)->result();
    // return $this->db->get($this->table)->result();
  }

  public function get_jadwal($tahun, $dosen)
  {
  	$sql = "select * from jadwal join makul on jadwal.kode_makul=makul.kode_makul where jadwal.id_tahun_akademik=$tahun and jadwal.kode_dosen=$dosen order by nama_makul asc";
  	$this->db->query($sql)->result();
  }

}