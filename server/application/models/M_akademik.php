<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_akademik extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete_tahun_akademik($id)
  { 
    $this->db->where('id_tahun_akademik', $id);
    $this->db->delete('tahun_akademik');
    return true;
  }

  public function get_tahun_akademik($param = true)
  { 
    // if ($param === true) {
    //   $this->db->order_by('tahun', 'desc');
    //   $query = $this->db->get_where('tahun_akademik', array('status' => 1 ));
    //   return $query->row();
    // }
    $this->db->order_by('tahun', 'desc');
    $query = $this->db->get('tahun_akademik');
    return $query->result();
  }

  public function get_prodi()
  { 
    // $this->db->order_by('nama_prodi', 'asc');
    // $query = $this->db->get('prodi');
    // return $query->result();
    $sql = "SELECT prodi FROM mahasiswa GROUP BY prodi";
    return $this->db->query($sql)->result();
  }
  
  public function get_makul()
  { 
    $this->db->order_by('nama_makul', 'asc');
    $query = $this->db->get('makul');
    return $query->result();
  }

  public function get_setting()
  {
    return $this->db->get('setting')->num_rows();

  }

  public function update_setting($params)
  {
    $tahun_akademik = $this->input->post('tahun-akademik');
    $jenis_ujian = $this->input->post('jenis-ujian');
    $status = $this->input->post('status');

    $this->load->library('rsa');
    $keys = $this->rsa->generate_keys(); 
    if ($params == true) {
      $data = array(
      'id_tahun_akademik' => $tahun_akademik,
      'jenis_ujian' => $jenis_ujian,
      'status' => $status);
      $this->db->update('setting', $data);
    }else{
      $data = array(
      'id_tahun_akademik' => $tahun_akademik,
      'jenis_ujian' => $jenis_ujian,
      'modulo' => $keys[0],
      'public' => $keys[1],
      'private' => $keys[2],
      'status' => $status);
      $this->db->insert('setting', $data); 
    }
    return true;
  }


  public function ajax_get_setting()
  {
    $sql = "SELECT * FROM tahun_akademik JOIN setting ON setting.id_tahun_akademik=tahun_akademik.id_tahun_akademik";
    return $this->db->query($sql)->row();
  }

  public function ajax_get_krs($nim, $tahun)
  {
   // $this->db->join('kelas', 'kelas.id_kelas=krs.id_kelas');
   $this->db->join('makul', 'makul.kode_makul=krs.kode_makul');
   return $this->db->get_where('krs', array('krs.nim' => $nim, 'krs.id_tahun_akademik' => $tahun));
  }

  public function ajax_load_kelas($tahun, $makul)
  {
    $where = array('id_tahun_akademik' => $tahun, 'kode_makul' => $makul );
    $this->db->order_by('kelas', 'asc');
    return $this->db->get_where('jadwal',$where )->result();
  }

  public function ajax_cek_kelas($id)
  {
    $this->db->join('makul', 'makul.kode_makul=jadwal.kode_makul', 'right');
    // $this->db->join('prodi', 'prodi.id_prodi=kelas.id_prodi', 'right');
    $this->db->join('dosen', 'dosen.kode_dosen=jadwal.kode_dosen', 'right');
    $this->db->join('tahun_akademik', 'tahun_akademik.id_tahun_akademik=jadwal.id_tahun_akademik', 'right');
    $this->db->where('jadwal.id_jadwal', $id);
    return $this->db->get_where('jadwal')->row();
  }

  public function ajax_cek_presensi($nim='', $id, $jenis)
  {

    return $this->db->get_where('presensi', array('nim' => $nim, 'id_kelas' => $id , 'jenis' => $jenis));
  }

  public function ajax_get_makul($smt)
  {
    return $this->db->get_where('makul',array('semester' => $smt ))->result();
  }

  public function ajax_load_jadwal($th, $hari)
  {
    $this->db->join('makul', 'makul.kode_makul=jadwal.kode_makul');
    $this->db->join('dosen', 'dosen.kode_dosen=jadwal.kode_dosen');
    $this->db->where(array('id_tahun_akademik' => $th, 'hari' => $hari));
    return $this->db->get('jadwal');
  }

  public function ajax_delete_jadwal($id)
  {
    $this->db->where('id_jadwal', $id);
    return $this->db->delete('jadwal');
  }

  public function ajax_get_mahasiswa_by_kelas($id)
  {
    $this->db->where('kelas', $id);
    return $this->db->get('mahasiswa')->result();
  }
}