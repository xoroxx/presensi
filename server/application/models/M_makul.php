<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_makul extends CI_Model{

  protected $table = 'makul';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete($id)
  {
    $this->db->where('kode_makul', $id);
    $this->db->delete($this->table);
  }

  public function add_makul()
  {
    $kode       = $this->input->post('kode');
    $nama      = $this->input->post('nama');
    $sks      = $this->input->post('sks');
    $smt      = $this->input->post('semester');
    $data = array(
      'kode_makul' => $kode, 
      'nama_makul' => ucwords($nama), 
      'sks' => $sks,
      'semester' => $smt);
    return $this->db->insert($this->table, $data);
  }

  public function get_makul()
  {
    return $this->db->get($this->table)->result();
  }

}