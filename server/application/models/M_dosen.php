<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dosen extends CI_Model{

  protected $table = 'dosen';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete($id)
  {
    $this->db->where('kode_dosen', $id);
    $this->db->delete($this->table);
  }

  public function add_dosen()
  {
    $nip       = $this->input->post('nip');
    $nama      = $this->input->post('nama');
    $username      = $this->input->post('username');
    $data = array('nip' => $nip, 'nama_dosen' => ucwords($nama), 'username' => $username, 'password' => sha1($username));
    return $this->db->insert($this->table, $data);
  }

  public function get_dosen()
  {
    // if ($params === true) {
          return $this->db->get($this->table)->result();
    // }
        // return $this->db->get_where($this->table, array('kode_dosen' => $params ))->row();
  }

  public function get_dosen_by_id($params)
  {
    // if ($params === true) {
          // return $this->db->get($this->table)->result();
    // }
        return $this->db->get_where($this->table, array('kode_dosen' => $params ))->row();
  }

  public function check_krs($tahun, $kode)
  {
    return $this->db->get_where('krs',array('id_kelas' => $kode, 'id_tahun_akademik' => $tahun))->result();
  }

  // public function FunctionName($)
  // {
  //   # code...
  // }
  public function get_kelas($tahun, $dosen)
  {
  	$sql = "select * from jadwal join makul on jadwal.kode_makul=makul.kode_makul where jadwal.id_tahun_akademik=$tahun and jadwal.kode_dosen=$dosen order by nama_makul asc";
  	return $this->db->query($sql)->result();
  }

  public function get_setting()
  {
    return $this->db->get('setting')->row();
  }


  public function update_password($value='')
  {
    $password = $this->input->post('password');
    $this->db->where('kode_dosen', $value);
    $this->db->update('dosen',array('password' => sha1($password)));
    return true;
  }

  public function ajax_cek_presensi($nim, $id, $jenis)
  {

    return $this->db->get_where('presensi', array('nim' => $nim, 'id_jadwal' => $id , 'jenis' => $jenis));
  }

  public function ajax_get_krs_mhs($makul, $kelas, $tahun)
  {
    $this->db->join('mahasiswa', 'mahasiswa.nim=krs.nim');
    $this->db->where('mahasiswa.kelas', $kelas);
    $this->db->where('krs.kode_makul', $makul);
    $this->db->where('krs.id_tahun_akademik', $tahun);
    $this->db->order_by('krs.nim', 'asc');
    return $this->db->get('krs')->result();
  }

   public function ajax_load_jadwal($th, $hari, $dosen)
  {
    $this->db->join('makul', 'makul.kode_makul=jadwal.kode_makul');
    // $this->db->join('dosen', 'dosen.kode_dosen=jadwal.kode_dosen');
    $this->db->where(array('id_tahun_akademik' => $th, 'hari' => $hari, 'kode_dosen' => $dosen));
    return $this->db->get('jadwal');
  }

  public function do_presensi($params = array())
  {
    $this->db->insert('presensi', $params);
    return true;
  }
}