<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makul extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_makul', 'makul');
	}


	public function index()
	{
		$data['makul'] = $this->makul->get_makul();
		$this->template->content->view('akademik/view_makul', $data);
        $this->template->publish();
	}

	public function delete()
	{	
		$id = $this->uri->segment(3);
		$this->makul->delete($id);
		redirect('makul');
	}

	public function add()
	{
		if ($this->input->post()) {
			$this->makul->add_makul();
		}
		$this->template->content->view('akademik/add_makul');
        $this->template->publish();
	}


}
