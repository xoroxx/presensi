<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akademik extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_akademik', 'akademik');
		$this->load->model('m_mahasiswa', 'mahasiswa');
	}

	
	
	/**
	 * mahasiswa controller
	 */
	public function view_mahasiswa()
	{
		$data['prodi'] = $this->akademik->get_prodi();
		$data['mahasiswa'] = $this->mahasiswa->get_mahasiswa();
		$this->template->content->view('akademik/view_mahasiswa', $data);
        $this->template->publish();
	}

	public function add_mahasiswa()
	{
		// if ($this->input->post('submit')) {
		// 	$query = $this->mahasiswa->add_mahasiswa();
		// 	if (isset($query)) {
		// 		redirect('akademik/add', 'refresh');
		// 	}
		// }
		$data['prodi'] = $this->akademik->get_prodi();
		$this->template->content->view('akademik/add_mahasiswa', $data);
        $this->template->publish();
	}

	public function save_mahasiswa()
	{
		$query = $this->mahasiswa->add_mahasiswa();
		if (isset($query)) {
			redirect('mahasiswa/add');
		}
	}
	/**end Mahasiswa Controller*/

	/**
	 * Akademik controller
	 */
	public function load_tahun_akademik()
	{
		$result = $this->akademik->get_tahun_akademik();
		foreach ($result as $key) {
			echo "<option value=".$key->id_tahun_akademik."";
			echo "".$key->status==1?"selected>":">"."";
			echo "".$key->tahun." - ";
			echo "".$key->periode==1?'Ganjil':'Genap'."</option>";
		}
	}
	/***end Akademik Controller*/

	public function setting()
	{
		$setting = $this->akademik->get_setting();
		$data['tahun_akademik'] = $this->akademik->get_tahun_akademik();
		$this->template->content->view('akademik/view_setting', $data);
        $this->template->publish();
	}

	public function update_setting()
	{
		$setting = $this->akademik->get_setting();
		if ($setting > 0) {
			$result = $this->akademik->update_setting(true);
		}else{
			$result = $this->akademik->update_setting(false);
		}
		redirect('akademik/setting');
	}

	public function view_tahun_akademik()
	{
		$data['tahun_akademik'] = $this->akademik->get_tahun_akademik();
		$this->template->content->view('akademik/view_tahun_akademik', $data);
        $this->template->publish();
	}

	public function delete_tahun_akademik()
	{
		$id = $this->uri->segment(3);
		$result = $this->akademik->delete_tahun_akademik($id);
		if ($result) {
			redirect('akademik/view_tahun_akademik');
		}
	}
}
