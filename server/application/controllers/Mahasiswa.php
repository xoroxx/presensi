<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_mahasiswa', 'mahasiswa');
		$this->load->model('m_mahasiswa', 'akademik');
		$this->session->set_userdata('type', 1);
	}
	public function index()
	{
		// $data['prodi'] = $this->akademik->get_prodi();
		$data['mahasiswa'] = $this->mahasiswa->get_mahasiswa();
		$this->template->content->view('akademik/view_mahasiswa', $data);
        $this->template->publish();
	}

	public function add()
	{
		if ($this->input->post()) {
			$this->mahasiswa->add_mahasiswa();
		}
		// // $data['prodi'] = $this->akademik->get_prodi();
		$this->template->content->view('akademik/add_mahasiswa');
        $this->template->publish();
  		
	}

	public function delete()
	{
		$this->mahasiswa->delete();
		redirect('mahasiswa');
	}
	// public function view()
	// {
	// 	$prodi = $this->uri->segment(3);
	// 	$data['mahasiswa'] = $this->mahasiswa->get_mahasiswa_by_prodi($prodi);
	// 	$this->template->content->view('akademik/view_mahasiswa', $data);
 //        $this->template->publish();
	// }
}
