<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_kelas', 'kelas');
		$this->load->model('m_akademik', 'akademik');
	}


	public function index()
	{
		// if ($this->input->post()) {
		// 	$id = $this->input->post('tahun-akademik');
		// 	$data['kelas'] = $this->kelas->get_kelas($id);

		// }
		$data['tahun_akademik'] = $this->kelas->get_tahun_akademik();
		$this->template->content->view('akademik/view_kelas', $data);
        $this->template->publish();
	}

	public function delete()
	{	
		$id = $this->uri->segment(3);
		$this->makul->delete($id);
		redirect('makul');
	}

	public function add()
	{
		$setting = $this->kelas->get_setting_app();
		if ($this->input->post()) {
			$this->kelas->add_kelas($setting->id_tahun_akademik);
			$this->session->set_flashdata('result', true);
		}
		
		
		$this->load->model(array('m_makul' => 'makul', 'm_dosen' => 'dosen', 'm_prodi' => 'prodi'));
		$data['makul'] = $this->makul->get_makul();
		$data['dosen'] = $this->dosen->get_dosen(false);
		$data['prodi'] = $this->prodi->get_prodi();
		
		
		$this->template->content->view('akademik/add_jadwal', $data);
        $this->template->publish();
	}


}
