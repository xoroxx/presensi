<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_presensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_akademik', 'akademik');
		$this->load->model('m_dosen', 'dosen');
	}
	
	public function load_data_kelas()
	{
		$tahun = $this->input->post('tahun');
		$result = $this->akademik->ajax_load_kelas($tahun);
		if ($result) {
			foreach ($result as $key) {
				echo "<option onclick='load_data_krs($key->nim)' >".$key->nim." - ".$key->nama_mahasiswa."</option>";
			}
		}
	}

	public function do_presensi()
	{
		$data 	= $this->input->post('data');
		$jadwal = $this->input->post('jadwal');
		//get keys to setting data tables
		$keys 	= $this->dosen->get_setting();
		//load encryption library and do descryption
		// $num = intval(trim($data, " "));
		// if (is_numeric($num)) {
		// 	echo "<script>alert(IS NUMERIC)</script>";
		// }else{
		$this->load->library('rsa'); 
		$decoded = $this->rsa->rsa_decrypt(base64_decode($data),  $keys->private,  $keys->modulo); //
		if ($decoded) {
				$row = explode("-", $decoded);
			if ($row[1] != $keys->id_tahun_akademik) {
		        // echo "<h1>WARNING</h1>";
				// echo "<p>Kartu Tidak Valid</p>";
				echo json_encode(array('result' =>  'warning'));
			}else{ 
				$this->load->model('m_mahasiswa', 'mahasiswa');
				//cek data mahasiswa dan presensi
				$mhs = $this->mahasiswa->get_mahasiswa_by_nim($row[0]);
				$cek = $this->mahasiswa->cek_presensi($row[0], $jadwal);
				if ($cek) {
					echo json_encode(array('result' =>  'tersimpan', 'nim' => $row[0], 'nama' => $mhs->nama_mahasiswa));
		            // echo "<h1>".$row[0]."</h1>";
		            // echo "<p><b>".$mhs->nama_mahasiswa."</b> Sudah Tersimpan</p>";
				}else{
					$params = array(
						'nim' => $row[0], 
						'jenis' => $keys->jenis_ujian, 
						'id_jadwal' => $jadwal, 
						'jam' => date('H:i'));	
					$this->dosen->do_presensi($params);
		            // echo "<h1>".$row[0]."</h1>";
					// echo "<p><b>".$mhs->nama_mahasiswa."</b><p>Hadir</p>";
					echo json_encode(array('result' =>  'success', 'nim' => $row[0], 'nama' => $mhs->nama_mahasiswa));
				}
			}

		}
	}
	
}

