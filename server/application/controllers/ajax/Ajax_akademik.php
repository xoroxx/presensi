<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_akademik extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_akademik', 'akademik');
	}
	
	public function load_data_kelas()
	{
		$tahun = $this->input->post('tahun');
		$makul = $this->input->post('makul');
		$result = $this->akademik->ajax_load_kelas($tahun, $makul);
		if (isset($result)) {
			foreach ($result as $key) {
				echo "<option onclick='load_data_presensi($key->id_jadwal, $tahun)' >".strtoupper($key->kelas)."</option>";
			}
		}
	}


	public function load_data_presensi()
	{
		$id = $this->input->post('id');
		$tahun = $this->input->post('tahun');
		$data = $this->akademik->ajax_cek_kelas($id);
		if (isset($data)) {
			$periode = $data->periode==1?'Ganjil':'Genap';
			echo "
	        <table >
	        <tr>
	            <td width=100>Nama Makul</td><td width=300>: ".  ucwords($data->nama_makul)."</td>
	            <td>Dosen</td><td>: ".  ucwords($data->nama_dosen)."</td>
	        </tr>
	        <tr>
	        	<td>Kelas</td><td>: ".  ucwords($data->kelas)."</td>
	            <td width=150>Tahun Akademik </td><td>: ".  $data->tahun." - ".$periode."</td>   
	        </tr>
       		</table>";

       		echo "<div class=ln_solid></div>";
        	
       		
       		echo "<div class='row'>";
       		echo "<div class='col-md-6'>";
       		echo "
       		<a href=".base_url()."presensi/print_uts/".$id." class='btn btn-xs btn-primary' target='_blank' ><i class='fa fa-print'></i> Print UTS</a>
	        <table class='table table-bordered table-responsive'>
	        <thead>
		        <tr>
		        <th width='5'>No</th>
		        <th>NIM</th>
		        <th>NAMA MAHASIWA</th>
		        <th width=50 align='center'>STATUS</th>
		    </thead>
		    <tbody>";
		    $this->load->model('m_dosen', 'dosen');
		    $krs = $this->dosen->ajax_get_krs_mhs($data->kode_makul, $data->kelas, $tahun);
			$setting = $this->dosen->get_setting();
			if (isset($krs)) {
				$no =1;
				foreach ($krs as $key) {
					$cek = $this->dosen->ajax_cek_presensi($key->nim, $id, 1);
					$status = $cek->num_rows() > 0 ?'<a class="text-success">Hadir</a>':'<a class="text-danger">Tidak</a>';
					echo "<tr>";
					echo "<td>".$no."</td>";
					echo "<td>".$key->nim."</td>";
					echo "<td>".$key->nama_mahasiswa."</td>";
					echo "<td>".$status."</td>";
					echo "</tr>";
					$no++;
				}
				
			}
			echo "</tbody>
			</table>";
			echo "</div>";
			echo "<div class='col-md-6'>";
       		echo "
       		<a href=".base_url()."presensi/print_uas/".$id." class='btn btn-xs btn-primary' target='_blank'><i class='fa fa-print'></i> Print UAS</a>
	        <table class='table table-bordered table-responsive'>
	        <thead>
		        <tr>
		        <th width='5'>No</th>
		        <th>NIM</th>
		        <th>NAMA MAHASIWA</th>
		        <th width=50 align='center'>STATUS</th>
		    </thead>
		    <tbody>";
		    $this->load->model('m_dosen', 'dosen');
		    $krs = $this->dosen->ajax_get_krs_mhs($data->kode_makul, $data->kelas, $tahun);
			$setting = $this->dosen->get_setting();
			if (isset($krs)) {
				$no =1;
				foreach ($krs as $key) {
					$cek = $this->dosen->ajax_cek_presensi($key->nim, $id, 2);
					$status = $cek->num_rows() > 0 ?'<a class="text-success">Hadir</a>':'<a class="text-danger">Tidak</a>';
					echo "<tr>";
					echo "<td>".$no."</td>";
					echo "<td>".$key->nim."</td>";
					echo "<td>".$key->nama_mahasiswa."</td>";
					echo "<td>".$status."</td>";
					echo "</tr>";
					$no++;
				}
				
			}
			echo "</tbody>
			</table></div>";
			// echo "</div>";
			// echo "<div class='col-md-6'>";
   //     		echo "
   //     		<a href=".base_url()."presensi/print_uas/".$id." class='btn btn-xs btn-primary'><i class='fa fa-print'></i> Print UAS</a>
	  //       <table class='table table-bordered table-responsive'>
	  //       <thead>
		 //        <tr>
		 //        <th width='5'>No</th>
		 //        <th>NIM</th>
		 //        <th>NAMA MAHASIWA</th>
		 //        <th width=50 align='center'>STATUS</th>
		 //    </thead>
		 //    <tbody>";
   //     		$this->load->model('m_dosen', 'dosen');
			// $krs = $this->dosen->ajax_get_krs_mhs($id);
			// if (isset($krs)) {
			// 	$no =1;
			// 	foreach ($krs as $key) {
			// 		$cek = $this->akademik->ajax_cek_presensi($key->nim, $id, 2);
			// 		$status = $cek->num_rows() > 0 ?'<a class="text-success">Hadir</a>':'<a class="text-danger">Tidak</a>';
			// 		echo "<tr>";
			// 		echo "<td>".$no."</td>";
			// 		echo "<td>".$key->nim."</td>";
			// 		echo "<td>".$key->nama_mahasiswa."</td>";
			// 		echo "<td align='center'>".$status."</td>";
			// 		echo "</tr>";
			// 		$no++;
			// 	}
				
			// }
			// echo "</tbody>
			// </table>";
			// echo "</div>";
			// echo "</div>";
		
		}
	}

	public function ajax_load_jadwal()
	{
		$tahun = $this->input->post('tahun');
		$hari = array('senin','selasa','rabu','kamis','jumat','sabtu','minggu');
		for ($i=0; $i < count($hari) ; $i++) { 
			$jadwal = $this->akademik->ajax_load_jadwal($tahun, $hari[$i]);
			if ($jadwal->num_rows() > 0) {
					
				// if (isset($jadwal->result())) {	
				echo "<tr>";
				// $tm = $jadwal->result();
				$row = $jadwal->num_rows() + 1;
				echo "<td rowspan=".$row.">".strtoupper($hari[$i])."</td>";
				foreach ($jadwal->result() as $key) {
					echo "<tr id=hide$key->id_jadwal>";
					echo "<td >".$key->nama_makul."</td>";
					echo "<td >".ucwords($key->kelas)."</td>";
					echo "<td >".$key->nama_dosen."</td>";
					echo "<td align=center ><a href='#' onclick='delete_jadwal($key->id_jadwal)'><i class='fa fa-trash-o'></i></a></td>";
					echo "</tr>";
				}
				// if (isset($jadwal->result())) {
				// 	echo "<tr>";
				// 	echo "<td collspan=".$jadwal->num_rows().">".$jadwal->hari."</td>";
				// 	echo "</tr>";
				// }
				echo "</tr>";
				// }
			}
		}
	}

	public function ajax_delete_jadwal()
	{
		$id = $this->input->post('id');
		$this->akademik->ajax_delete_jadwal($id);
	}
}
