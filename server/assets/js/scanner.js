// $(document).ready(function() {
  //   var scannerLaser = $(".scanner-laser").length;
  //   var arg = {
  //     // grayScale: 1,
  //     frameRate: 25,
  //     // contrast: 50,
  //     zoom: -1,
  //     beep: '<?php echo base_url()?>assets/audio/beep.mp3',
  //     decoderWorker: '<?php echo base_url()?>assets/js/DecoderWorker.js',
  //     flipHorizontal: true,
  //     resultFunction: function(res) {
  //         doPresensi(res.code);
  //     }
  //   };

  //   //Initialize new Class
  //   var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
  //   decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg);
  //   //if Button click
  //   $("#btn-scanner").click(function () {
  //     if ($(this).html() == 'ON') {
  //       if (!decoder.isInitialized()) {
  //           console.log('Scanning...');
  //       } else {
  //           console.log('Scanning...');
  //           decoder.play();   
  //           $(this).html('OFF');
  //       }   
  //     }else{
  //         decoder.stop();
  //         $(this).html('ON');
  //     }
  //   });
  //   //if Select change
  //   $("[name=kelas]").change(function () {
  //     if (decoder.isInitialized()) {
  //         decoder.stop();
  //         $("#btn-scanner").html('ON');
  //     }
  //   });
  //   //do save presensi data
  //   function doPresensi(data) {
  //     var jadwal = $("[name=kelas]").val();
  //     $.ajax({
  //       url: "<?php echo base_url()?>ajax/ajax_presensi/do_presensi",
  //       data: {'data':data, 'jadwal':jadwal},
  //       type: "html",
  //       dataType: "json",
  //       success: function (respone) {
  //         $(".bs-example").html(respone);
  //       },
  //       error: function (error) {
  //         console.log(error)
  //       }
  //     });
  //   }
  // });

(function(undefined) {
    "use strict";
    function Q(el) {
        if (typeof el === "string") {
            var els = document.querySelectorAll(el);
            return typeof els === "undefined" ? undefined : els.length > 1 ? els : els[0];
        }
        return el;
    }
    var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
    //html element
    var scannerLaser = Q(".scanner-laser"),
        play = Q("#btn-scanner");
    //options library
    var args = {
        resultFunction: function(res) {
            [].forEach.call(scannerLaser, function(el) {
                fadeOut(el, 0.5);
                setTimeout(function() {
                    fadeIn(el, 0.5);
                }, 300);
            });
            alert(res.format + ": " + res.code);
        },
        getDevicesError: function(error) {
            var p, message = "Error detected with the following parameters:\n";
            for (p in error) {
                message += p + ": " + error[p] + "\n";
            }
            alert(message);
        },
        getUserMediaError: function(error) {
            var p, message = "Error detected with the following parameters:\n";
            for (p in error) {
                message += p + ": " + error[p] + "\n";
            }
            alert(message);
        },
        cameraError: function(error) {
            var p, message = "Error detected with the following parameters:\n";
            if (error.name == "NotSupportedError") {
                confirm("Your browser does not support getUserMedia via HTTP!\n(see: https:goo.gl/Y0ZkNV).\n");
            } else {
                for (p in error) {
                    message += p + ": " + error[p] + "\n";
                }
                alert(message);
            }
        },
        cameraSuccess: function() {
            console.log('Camera ON');;
        }
    };

    //load new Class from Library
    var decoder = new WebCodeCamJS("#webcodecam-canvas").buildSelectMenu(document.createElement('select'), "environment|back").init(args);
    
    window.onload = function() {
            // decoder.play();
    }
    //function button on/of scanner
    play.addEventListener("click", function() {
        if (play[txt] == 'ON') {
            if (!decoder.isInitialized()) {
                console.log('Scanning...');
            } else {
                console.log('Scanning...');
                decoder.play();   
                play[txt] = 'OFF';
            }   
        }else{
            decoder.stop();
            play[txt] = 'ON';
        }
    }, false);
    
    //animate scanner
    function fadeOut(el, v) {
        el.style.opacity = 1;
        (function fade() {
            if ((el.style.opacity -= 0.1) < v) {
                el.style.display = "none";
                el.classList.add("is-hidden");
            } else {
                requestAnimationFrame(fade);
            }
        })();
    }

    function fadeIn(el, v, display) {
        if (el.classList.contains("is-hidden")) {
            el.classList.remove("is-hidden");
        }
        el.style.opacity = 0;
        el.style.display = display || "block";
        (function fade() {
            var val = parseFloat(el.style.opacity);
            if (!((val += 0.1) > v)) {
                el.style.opacity = val;
                requestAnimationFrame(fade);
            }
        })();
    }

    document.querySelector("[name=kelas]").addEventListener("change", function() {
        if (decoder.isInitialized()) {
            decoder.stop();
            play[txt] = 'ON';
        }
    });

}).call(window.Page = window.Page || {});
